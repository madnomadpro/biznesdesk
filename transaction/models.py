from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class Transaction(models.Model):
    transaction_status = (
        ('accepted', 'Подтвержден'),
        ('rejected', 'Отклонен'),
        ('awaiting', 'В ожидании')
    )
    user = models.OneToOneField(User, related_name='transaction')
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    date = models.DateTimeField(auto_now_add=True)
    status = models.CharField(choices=transaction_status, max_length=6)
