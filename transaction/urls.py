from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.create_signature, name='signature'),
    url(r'^confirm/$', views.transaction_treatment, name='transaction_treatment'),
    url(r'^success/$', views.payment_success, name='success'),
    url(r'^fail/$', views.payment_fail, name='fail'),
]
