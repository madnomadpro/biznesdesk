import json
from hashlib import md5
from collections import defaultdict
import binascii

from django.contrib import messages
from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from main import settings
from transaction.models import Transaction


def get_signature(w1_data):
    secret_key = settings.WMI_SECRET_KEY
    icase_key = lambda s: s.lower()
    lists_by_keys = defaultdict(list)
    for key, value in iter(w1_data.items()):
        lists_by_keys[key].append(value)
    str_buff = b''
    for key in sorted(lists_by_keys, key=icase_key):
        for value in sorted(lists_by_keys[key], key=icase_key):
            str_buff += value.encode('1251')
    str_buff += bytes(secret_key, encoding='1251')
    md5_string = md5(str_buff).digest()
    return binascii.b2a_base64(md5_string)[:-1].decode()


@csrf_exempt
def create_signature(request):
    w1_data = {
        'WMI_MERCHANT_ID': request.POST.get('WMI_MERCHANT_ID'),
        'WMI_PAYMENT_AMOUNT': request.POST.get('WMI_PAYMENT_AMOUNT'),
        'WMI_CURRENCY_ID': request.POST.get('WMI_CURRENCY_ID'),
        'WMI_DESCRIPTION': request.POST.get('WMI_DESCRIPTION'),
        'WMI_SUCCESS_URL': request.POST.get('WMI_SUCCESS_URL'),
        'WMI_FAIL_URL': request.POST.get('WMI_FAIL_URL'),
    }
    return HttpResponse(get_signature(w1_data))


@csrf_exempt
def transaction_treatment(request):
    if request.method == 'POST':
        if not request.POST.get('WMI_SIGNATURE'):
            wmi_data = w1_print_answer("RETRY", "No_WMI_SIGNATURE")
        elif not request.POST.get('WMI_PAYMENT_NO'):
            wmi_data = w1_print_answer("RETRY", "No_WMI_PAYMENT_NO")
        elif not request.POST.get("WMI_ORDER_STATE"):
            wmi_data = w1_print_answer("RETRY", "No_WMI_ORDER_STATE")
        else:
            post_data = request.POST.copy()

            for key in post_data.keys():
                if key == 'WMI_SIGNATURE':
                    del post_data[key]

            signature = get_signature(post_data)
            if request.POST.get('WMI_SIGNATURE') == signature:
                amount = request.POST.get('WMI_PAYMENT_AMOUNT')
                status = request.POST.get('WMI_ORDER_STATE').lower()
                if status == 'accepted':
                    request.user.profile.balance += int(amount)
                    request.user.profile.save()
                    wmi_data = w1_print_answer('OK', 'ORDER %s PAID' % request.POST.get('WMI_ORDER_ID'))
                    transaction_status(request.user, amount, status)
                elif status == 'rejected':
                    transaction_status(request.user, amount, status)
                    wmi_data = w1_print_answer("RETRY", "SOME_ERROR_ORDER_STATE_" + request.POST.get("WMI_ORDER_STATE"))
                else:
                    wmi_data = w1_print_answer("RETRY", "SOME_ERROR_ORDER_STATE_" + request.POST.get("WMI_ORDER_STATE"))
            else:
                wmi_data = w1_print_answer("RETRY", "Not_correct_SIGNATURE ")
        return HttpResponse(wmi_data)


def w1_print_answer(status, description=None):
    if description:
        return 'WMI_RESULT=' + status + '&WMI_DESCRIPTION=' + description
    else:
        return 'WMI_RESULT=' + status


def transaction_status(user, amount, status):
    Transaction.objects.create(user=user, amount=amount, status=status)


def payment_success(request):
    messages.add_message(request, messages.SUCCESS, 'Баланс успешно пополнен!')
    return reverse('profile:general')


def payment_fail(request):
    messages.add_message(request, messages.WARNING, 'При пополнении баланса произошла ошибка!')
    return reverse('profile:general')
