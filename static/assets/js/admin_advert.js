/**
 * Created by stas on 01.05.16.
 */
$(document).ready(function () {
    var $type = $('#id_type');
    var $act = $('.field-act');
    var $multiple = $('.field-multiple_division');
    var $single = $('.field-single_division');
    var $single_input = $('#id_single_division');
    var $multiple_input = $('#id_multiple_division');
    var $single_error = $single.find('ul.errorlist').html();
    var $multiple_error = $multiple.find('ul.errorlist').html();
    if (($single_input.val() || $single_error) && !$multiple_error) {
        $single.show();
        $multiple.hide();
    } else if (($multiple_input.val() || $multiple_error) && !$single_error) {
        $multiple.show();
        $single.hide();
    } else {
        $single.hide();
        $multiple.hide();
    }

    if ($type.val() == 'ready') {
        $act.show()
    } else {
        $act.hide()
    }
    $type.click(function () {
        if (this.value == 'offer') {
            $multiple.show();
            $single.hide();
        } else {
            $multiple.hide();
            $single.show()
        }
        if (this.value == 'ready') {
            $act.show()
        } else {
            $act.hide()
        }
    })
});