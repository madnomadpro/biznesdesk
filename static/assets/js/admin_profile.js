/**
 * Created by stas on 29.04.16.
 */
$(document).ready(function () {
    var $region = $('#id_region');
    var $field = $('.field-region');
    var $partner = $('#id_partner');
    if (!$partner.is(':checked')) {
        $field.css({'display': 'none'});
    }
    $partner.click(function () {
        if ($(this).is(':checked')) {
            $field.css({'display': 'block'});
        } else {
            $field.css({'display': 'none'});
            $region.val(null);
        }
    })
});