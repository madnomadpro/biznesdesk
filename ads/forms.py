# coding=utf-8
from django import forms
from django.contrib.auth.models import User
from django.core import validators

from .models import Advert, Region, sectors, acts, AdvertPhoto, Shop, Division, Billboard


class AdvertCreateForm(forms.ModelForm):
    single_division = forms.ModelChoiceField(Division.objects.all(), empty_label=None, required=False)
    region = forms.ModelChoiceField(Region.objects.all(), empty_label=None)
    type = forms.ChoiceField(choices=sectors)
    act = forms.ChoiceField(choices=acts)
    cost = forms.CharField(max_length=25)
    phone = forms.CharField(
        validators=[validators.RegexValidator(
            r'^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$',
            'Введите правильный номер телефона'
        )],
        label='Номер телефона')

    class Meta:
        model = Advert
        exclude = ['url_name', 'owner', 'active', 'shop', 'solve_reject_text', 'status', 'position', 'cost']

    def clean(self):
        cleaned_data = super(AdvertCreateForm, self).clean()
        # if not cleaned_data.get('multiple_division') and cleaned_data.get('type') == 'offer':
        # raise forms.ValidationError({'multiple_division': 'Выберите хотя бы одну категорию'})
        if not cleaned_data.get('single_division') and cleaned_data.get('type') != 'offer':
            raise forms.ValidationError({'single_division': 'Выберите категорию'})

    def __init__(self, *args, **kwargs):
        instance = kwargs.get('instance', None)
        if instance:
            kwargs.update(initial={
                'cost': instance.cost,
            })
        super(AdvertCreateForm, self).__init__(*args, **kwargs)


class AdvertPhotoForm(forms.ModelForm):
    class Meta:
        model = AdvertPhoto
        fields = ['photo']


PhotoFormSet = forms.inlineformset_factory(Advert, AdvertPhoto, form=AdvertPhotoForm, can_delete=True, max_num=10,
                                           extra=2)


class AdvertAdminForm(forms.ModelForm):
    owner = forms.ModelChoiceField(User.objects.all(), empty_label=None)
    region = forms.ModelChoiceField(Region.objects.all(), empty_label=None)
    type = forms.ChoiceField(choices=sectors)
    act = forms.ChoiceField(choices=acts)

    class Meta:
        model = Advert
        exclude = ['url_name', ]

    def clean(self):
        cleaned_data = super(AdvertAdminForm, self).clean()
        if not cleaned_data.get('multiple_division') and cleaned_data.get('type') == 'offer':
            raise forms.ValidationError({'multiple_division': 'Выберите хотя бы одну категорию'})
        elif not cleaned_data.get('single_division') and cleaned_data.get('type') != 'offer':
            raise forms.ValidationError({'single_division': 'Выберите категорию'})


class ShopCreateForm(forms.ModelForm):
    image = forms.CharField(widget=forms.HiddenInput, required=False)
    region = forms.ModelChoiceField(Region.objects.all(), empty_label=None)

    class Meta:
        model = Shop
        exclude = ['owner', 'status', 'active_days', 'paid']


class BillboardCreateForm(forms.ModelForm):
    region = forms.ModelChoiceField(Region.objects.all(), required=False, empty_label='На всю Россию')

    class Meta:
        model = Billboard
        exclude = ['user', 'shows', 'clicks', 'status', 'show']


class BillboardUpdateForm(forms.ModelForm):
    region = forms.ModelChoiceField(Region.objects.all(), required=False, empty_label='На всю Россию')

    class Meta:
        model = Billboard
        exclude = ['user', 'shows', 'clicks', 'status']
