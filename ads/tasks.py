from datetime import timedelta

from celery.schedules import crontab
from celery.task import periodic_task
from celery.utils.log import get_task_logger

from .models import Shop
logger = get_task_logger(__name__)


@periodic_task(run_every=(crontab(minute='0', hour='0')))
def shop_days():
    shops = Shop.objects.filter(status='open', paid=True)
    for shop in shops:
        shop.active_days -= 1
        if shop.active_days == 0:
            shop.status = 'close'
            shop.paid = False
        shop.save()
