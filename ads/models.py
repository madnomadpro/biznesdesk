import os

from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_delete
from django.dispatch import receiver
from .utils import translit


# Create your models here.


class Division(models.Model):
    name = models.CharField(max_length=255, verbose_name='Название раздела')
    url_name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Раздел'
        verbose_name_plural = 'Разделы'

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.url_name = translit(self.name)
        super(Division, self).save()


class Region(models.Model):
    name = models.CharField(max_length=40, unique=True, verbose_name='Название региона')
    url_name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.url_name = translit(self.name)
        super(Region, self).save()

    class Meta:
        verbose_name = 'Регион'
        verbose_name_plural = 'Регионы'


class Metro(models.Model):
    region = models.ForeignKey(Region, verbose_name='Регион')
    name = models.CharField(max_length=60, verbose_name='Название метро')
    url_name = models.CharField(max_length=60)

    class Meta:
        verbose_name = 'Метро'
        verbose_name_plural = 'Метро'
        unique_together = ('region', 'name')

    def __str__(self):
        return self.name

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.url_name = translit(self.name)
        super(Metro, self).save()


class County(models.Model):
    region = models.ForeignKey(Region, verbose_name='Регоин')
    name = models.CharField(max_length=40, verbose_name='Название района')
    url_name = models.CharField(max_length=40)

    def __str__(self):
        return self.name

    class Meta:
        unique_together = ('region', 'name')
        verbose_name = 'Район'
        verbose_name_plural = 'Районы'

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.url_name = translit(self.name)
        super(County, self).save()


divisions = [
    Division(name='Автомойки, Автосервисы, СТО', url_name=translit('Автомойки, Автосервисы, СТО')),
    Division(name='Автозаправочные станции(АЗС)', url_name=translit('Автозаправочные станции(АЗС)')),
    Division(name='Автосалоны', url_name=translit('Автосалоны')),
    Division(name='Аптеки', url_name=translit('Аптеки')),
    Division(name='Арендный бизнес', url_name=translit('Арендный бизнес')),
    Division(name='Банковская деятельность', url_name=translit('Банковская деятельность')),
    Division(name='Бизнес и Недвижимость за рубежом', url_name=translit('Бизнес и Недвижимость за рубежом')),
    Division(name='Гостиницы, отели, базы отдыха', url_name=translit('Гостиницы, отели, базы отдыха')),
    Division(name='Добыча полезных ископаемых', url_name=translit('Добыча полезных ископаемых')),
    Division(name='Заводы, промышленные помещения', url_name=translit('Заводы, промышленные помещения')),
    Division(name='Коммерческая недвижимость', url_name=translit('Коммерческая недвижимость')),
    Division(name='Медицинские центры и стоматологии', url_name=translit('Медицинские центры и стоматологии')),
    Division(name='Образование', url_name=translit('Образование')),
    Division(name='Продуктовые магазины, супермаркеты', url_name=translit('Продуктовые магазины, супермаркеты')),
    Division(name='Производство', url_name=translit('Производство')),
    Division(name='Развлечения', url_name=translit('Развлечения')),
    Division(name='Рестораны, Кафе, Бары', url_name=translit('Рестораны, Кафе, Бары')),
    Division(name='Салоны красоты', url_name=translit('Салоны красоты')),
    Division(name='Сельское хозяйство', url_name=translit('Сельское хозяйство')),
    Division(name='Сервис и услуги', url_name=translit('Сервис и услуги')),
    Division(name='СМИ', url_name=translit('СМИ')),
    Division(name='Столовые', url_name=translit('Столовые')),
    Division(name='Страховая деятельность', url_name=translit('Страховая деятельность')),
    Division(name='Строительство', url_name=translit('Строительство')),
    Division(name='Телекоммуникации, Интернет - магазины, IT',
             url_name=translit('Телекоммуникации, Интернет - магазины, IT')),
    Division(name='Торговля(оптовая, розничная)', url_name=translit('Торговля(оптовая, розничная)')),
    Division(name='Транспорт и услуги', url_name=translit('Транспорт и услуги')),
    Division(name='Туристический бизнес', url_name=translit('Туристический бизнес')),
    Division(name='Услуги для бизнеса и населения', url_name=translit('Услуги для бизнеса и населения')),
    Division(name='Фитнес центры', url_name=translit('Фитнес центры')),
    Division(name='Прочий бизнес', url_name=translit('Прочий бизнес')),
]

regions = [
    Region(name='Москва', url_name=translit('Москва')),
    Region(name='Санкт-Петербург', url_name=translit('Санкт-Петербург')),
    Region(name='Кемерово', url_name=translit('Кемерово')),
    Region(name='Новокузнецк', url_name=translit('Новокузнецк')),
    Region(name='Ростов-на-Дону', url_name=translit('Ростов-на-Дону')),
    Region(name='Тюмень', url_name=translit('Тюмень')),
    Region(name='Барнаул', url_name=translit('Барнаул')),
    Region(name='Екатеринбург', url_name=translit('Екатеринбург')),
    Region(name='Краснодар', url_name=translit('Краснодар')),
    Region(name='Новосибирск', url_name=translit('Новосибирск')),
    Region(name='Самара', url_name=translit('Самара')),
    Region(name='Уфа', url_name=translit('Уфа')),
    Region(name='Владивосток', url_name=translit('Владивосток')),
    Region(name='Иркутск', url_name=translit('Иркутск')),
    Region(name='Красноярск', url_name=translit('Красноярск')),
    Region(name='Омск', url_name=translit('Омск')),
    Region(name='Сургут', url_name=translit('Сургут')),
    Region(name='Хабаровск', url_name=translit('Хабаровск')),
    Region(name='Волгоград', url_name=translit('Волгоград')),
    Region(name='Казань', url_name=translit('Казань')),
    Region(name='Нижний Новгород', url_name=translit('Нижний Новгород')),
    Region(name='Пермь', url_name=translit('Пермь')),
    Region(name='Томск', url_name=translit('Томск')),
    Region(name='Челябинск', url_name=translit('Челябинск'))
]


def shop_name(instance, filename):
    return '/'.join([instance.alias, filename])


open = 'open'
close = 'close'
status_shop = (
    (open, 'Открытый'),
    (close, 'Закрытый')
)


class Shop(models.Model):
    region = models.ForeignKey(Region, related_name='shops')
    owner = models.OneToOneField(User, related_name='shop', verbose_name='Владелец')
    name = models.CharField(max_length=40, verbose_name='Название магазина')
    alias = models.CharField(max_length=20, unique=True, verbose_name='Ссылка')
    anons = models.CharField(max_length=100, verbose_name='Текст для анонса')
    description = models.TextField(max_length=5000, verbose_name='Описание магазина')
    site = models.URLField(null=True, blank=True, verbose_name='Сайт магазина')
    person = models.CharField(max_length=30, verbose_name='ФИО')
    logo = models.ImageField(upload_to=shop_name, verbose_name='Логотип')
    status = models.CharField(choices=status_shop, max_length=5, verbose_name='Статус')
    video = models.CharField(max_length=50, null=True, blank=True, verbose_name='Видео')
    phones = models.CharField(null=True, blank=True, max_length=17, verbose_name='Телефон')
    mode = models.CharField(max_length=40, null=True, blank=True, verbose_name='График работы')
    address = models.CharField(max_length=255, null=True, blank=True, verbose_name='Адрес')
    skype = models.CharField(max_length=20, null=True, blank=True, verbose_name='Логин skype')
    date = models.DateTimeField(auto_now_add=True, verbose_name='Дата открытия')
    background = models.ImageField(upload_to=shop_name, null=True, blank=True, verbose_name='Задний фон')
    paid = models.BooleanField(default=False, verbose_name='Оплачен')
    active_days = models.IntegerField(default=0, verbose_name='Количество активных дней')

    class Meta:
        verbose_name = 'Магазин пользователя'
        verbose_name_plural = 'Магазины пользователей'

    def __str__(self):
        return '%s (%s)' % (self.name, self.alias)


class ShopBackground(models.Model):
    image = models.ImageField(upload_to='backgrounds/')

    def image_tag(self):
        return '<img src="%s" style="width: 200px; height: 100px;">' % self.image.url

    image_tag.allow_tags = True

    def __str__(self):
        return self.image.path

    class Meta:
        verbose_name = 'Задний фон магазина'
        verbose_name_plural = 'Задние фоны магазина'


ready = 'ready'
search = 'search'
offer = 'offer'

sectors = (
    (ready, 'Готовый бизнес'),
    (search, 'Поиск инвестиций'),
    (offer, 'Предложения инвесторов'),
)

acts = (
    ('sell', 'Продать'),
    ('buy', 'Купить'),
)

status = (
    ('consideration', 'На рассмотрении'),
    ('rejected', 'Отклонен'),
    ('accepted', 'Разрешен')
)


class PositionManager(models.Manager):
    def max_position(self):
        adverts = Advert.objects.all()
        if adverts:
            position = max(advert.position for advert in adverts)
        else:
            position = 1
        return position


class Favorite(models.Model):
    key = models.CharField(max_length=30, null=True, blank=True)
    user = models.OneToOneField(User, related_name='favorite', null=True, blank=True)


class Advert(models.Model):
    owner = models.ForeignKey(User, related_name='advert', verbose_name='Владелец')
    position = models.IntegerField(null=True, blank=True, default=None)
    region = models.ForeignKey(Region, verbose_name='Регион')
    metro = models.ForeignKey(Metro, null=True, blank=True, verbose_name='Метро')
    county = models.ForeignKey(County, null=True, blank=True, verbose_name='Округ')
    type = models.CharField(max_length=6, choices=sectors, verbose_name='Тип')
    multiple_division = models.ManyToManyField(Division, related_name='multiple', blank=True, verbose_name='Разделы')
    single_division = models.ForeignKey(Division, null=True, blank=True, related_name='single', verbose_name='Раздел')
    act = models.CharField(max_length=20, choices=acts, null=True, blank=True, verbose_name='Купить/Продать')
    title = models.CharField(max_length=70, verbose_name='Заголовок объявления')
    description = models.TextField(verbose_name='Описание', max_length=5000)
    cost = models.IntegerField(verbose_name='Стоимость')
    url_name = models.CharField(max_length=30)
    active = models.BooleanField(default=False)
    shop = models.ForeignKey(Shop, related_name='shop_item', null=True, blank=True, verbose_name='Магазин')
    on_slider = models.BooleanField(default=False)
    video = models.CharField(max_length=50, null=True, blank=True, verbose_name='Видео')
    phone = models.CharField(max_length=16, verbose_name='Телефон')
    status = models.CharField(choices=status, max_length=13, default='consideration', verbose_name='Статус объявления')
    solve_reject_text = models.TextField(null=True, blank=True, verbose_name='Причина отказа объявления')
    vip = models.BooleanField(default=False, verbose_name='Вип')
    allotted = models.BooleanField(default=False, verbose_name='Выделенное')
    f_search = models.BooleanField(default=False, verbose_name='Вверху поиска')
    date = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    favorite = models.ManyToManyField(Favorite, related_name='adverts', blank=True, verbose_name='Избранное')
    visitors = models.IntegerField(null=True, blank=True, verbose_name='Посещения за всё время', default=0)
    today = models.IntegerField(null=True, blank=True, verbose_name='Посещения за сегодня', default=0)
    last_visit = models.DateTimeField(null=True, blank=True, verbose_name='Последний просмотр')
    objects = PositionManager()
    show = models.BooleanField(default=True)

    def __str__(self):
        return '%s (%s)' % (self.title, self.owner)

    class Meta:
        verbose_name_plural = 'Объявления'
        verbose_name = 'Объявление'


def advert_photo(instance, filename):
    return '/'.join([str(instance.user.id), filename])


class AdvertPhoto(models.Model):
    advert = models.ForeignKey(Advert, related_name='photo', null=True, blank=True)
    user = models.ForeignKey(User, related_name='user_advert_photo', null=True, blank=True)
    photo = models.ImageField(upload_to=advert_photo)

    def __str__(self):
        if self.advert:
            return '%s (%s)' % (self.advert.title, self.advert.pk)
        else:
            return '%s' % self.user.username


class Billboard(models.Model):
    billboard_status = (
        ('accept', 'Разрешен'),
        ('prohibited', 'Запрещен'),
        ('consideration', 'На рассмотрении')
    )

    blocks = (
        ('1', 1),
        ('2', 2),
        ('3', 3),
        ('4', 4)
    )

    user = models.ForeignKey(User, related_name='billboards')
    region = models.ForeignKey(Region, related_name='b_region', null=True, blank=True)
    link = models.CharField(max_length=255)
    image = models.ImageField(upload_to='billboards/')
    shows = models.IntegerField(default=0)
    clicks = models.IntegerField(default=0)
    status = models.CharField(choices=billboard_status, default='consideration', max_length=13)
    block = models.CharField(max_length=1, choices=blocks)
    show = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Рекламный блок'
        verbose_name_plural = 'Рекламные блоки'


class ProjectSettings(models.Model):
    block_1 = models.IntegerField(default=0, verbose_name='Рекламный блок 1')
    width_1 = models.IntegerField(default=0, verbose_name='Ширина рекламного блока 1')
    height_1 = models.IntegerField(default=0, verbose_name='Высота рекламного блока 1')
    block_2 = models.IntegerField(default=0, verbose_name='Рекламный блок 2')
    width_2 = models.IntegerField(default=0, verbose_name='Ширина рекламного блока 2')
    height_2 = models.IntegerField(default=0, verbose_name='Высота рекламного блока 2')

    block_3 = models.IntegerField(default=0, verbose_name='Рекламный блок 3')
    width_3 = models.IntegerField(default=0, verbose_name='Ширина рекламного блока 3')
    height_3 = models.IntegerField(default=0, verbose_name='Высота рекламного блока 3')

    block_4 = models.IntegerField(default=0, verbose_name='Рекламный блок 4')
    width_4 = models.IntegerField(default=0, verbose_name='Ширина рекламного блока 4')
    height_4 = models.IntegerField(default=0, verbose_name='Высота рекламного блока 4')

    advert_up = models.IntegerField(default=0, verbose_name='Поднятие объявления')
    advert_vip = models.IntegerField(default=0, verbose_name='VIP объявление')
    advert_allotted = models.IntegerField(default=0, verbose_name='Выделение объявления')
    advert_search = models.IntegerField(default=0, verbose_name='Поднятие в поиске')
    shop = models.IntegerField(default=0, verbose_name='Стоимость магазина')
    active_days = models.IntegerField(default=0, verbose_name='Количество активных дней магазина')
    billboard = models.IntegerField(default=0, verbose_name='Стоимость клика')
    feedback_email = models.EmailField(verbose_name='Почта для принятия сообщений')
    checking_advert = models.BooleanField(default=True, verbose_name='Модерировать объявления?')

    class Meta:
        verbose_name = 'Настройки проекта'
        verbose_name_plural = 'Настройки проекта'

    def __str__(self):
        return 'Настройки проекта'


class Feedback(models.Model):
    advertising = models.BooleanField(default=False, verbose_name='Реклама')
    name = models.CharField(max_length=255)
    mail = models.EmailField()
    message = models.TextField()

    class Meta:
        verbose_name = 'Сообщение пользователя'
        verbose_name_plural = 'Сообщения пользователей'


@receiver(post_delete, sender=AdvertPhoto)
def img_delete(sender, instance, **kwargs):
    try:
        os.remove(instance.photo.path)
    except FileNotFoundError:
        pass
