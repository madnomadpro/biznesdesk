from django.contrib import admin
import datetime
from .utils import translit
from ads.forms import AdvertAdminForm
from .models import Region, Advert, Division, Shop, AdvertPhoto, Metro, Billboard, ProjectSettings, ShopBackground, \
    Feedback, County


# Register your models here.


class AdvertAdmin(admin.ModelAdmin):
    list_display = ('title', 'owner', 'region', 'type', 'status', 'vip', 'allotted', 'f_search')
    exclude = ('active', 'on_slider', 'favorite', 'position', 'last_visit')
    readonly_fields = ('visitors', 'today', 'vip', 'allotted', 'f_search')
    filter_horizontal = ('multiple_division', )
    list_filter = ('status', 'vip', 'allotted', 'f_search')
    search_fields = ('title', 'owner', 'description')

    form = AdvertAdminForm

    class Media:
        js = (
            'assets/js/jquery.min.js',
            'assets/js/admin_advert.js'
        )

    def save_model(self, request, obj, form, change):
        if obj.type == 'offer':
            obj.single_division = None
        else:
            form.cleaned_data['multiple_division'] = ''
        obj.date = datetime.datetime.now()
        obj.url_name = translit(obj.title)
        obj.video = form.cleaned_data['video'].replace('watch?v=', 'embed/')
        obj.save()


class RegionAdmin(admin.ModelAdmin):
    list_display = ('name', 'url_name', 'pk')
    fields = ('name', 'url_name')

    def get_queryset(self, request):
        return Region.objects.order_by('name')


class MetroAdmin(admin.ModelAdmin):
    list_display = ('name', 'region')


class ShopAdmin(admin.ModelAdmin):
    list_display = ('name', 'owner', 'status', 'paid', 'active_days')
    list_filter = ('status', 'paid')


class FeedbackAdmin(admin.ModelAdmin):
    list_display = ('name', 'mail', 'advertising')
    list_filter = ('advertising', )


class BackgroundAdmin(admin.ModelAdmin):
    fields = ('image', )
    list_display = ('pk', 'image', 'image_tag')


class SettingsAdmin(admin.ModelAdmin):
    fieldsets = (
        ('Рекламный блок 1', {
            'fields': ('block_1', 'width_1', 'height_1')
        }),
        ('Рекламный блок 2', {
            'fields': ('block_2', 'width_2', 'height_2')
        }),
        ('Рекламный блок 3', {
            'fields': ('block_3', 'width_3', 'height_3')
        }),
        ('Рекламный блок 4', {
            'fields': ('block_4', 'width_4', 'height_4')
        }),
        ('Плата за клик', {
            'fields': ('billboard', )
        }),
        ('Объявления', {
            'fields': ('advert_up', 'advert_allotted', 'advert_search', 'advert_vip', 'checking_advert')
        }),
        ('Магазины', {
            'fields': ('shop', 'active_days')
        }),
        ('Дополнительные настройки', {
            'fields': ('feedback_email', )
        })
    )


class DivisionAdmin(admin.ModelAdmin):
    fields = ('name', )


class BillboardAdmin(admin.ModelAdmin):
    list_display = ('user', 'region', 'block', 'link', 'shows', 'clicks', 'status')
    search_fields = ('link', )
    list_filter = ('status', )


class CountyAdmin(admin.ModelAdmin):
    list_display = ('name', 'region', 'pk')
    fields = ('region', 'name', )

    def get_queryset(self, request):
        return County.objects.order_by('name')


admin.site.register(Division, DivisionAdmin)
admin.site.register(Region, RegionAdmin)
admin.site.register(County, CountyAdmin)
admin.site.register(Advert, AdvertAdmin)
admin.site.register(Shop, ShopAdmin)
admin.site.register(Metro, MetroAdmin)
admin.site.register(Billboard, BillboardAdmin)
admin.site.register(ProjectSettings, SettingsAdmin)
admin.site.register(ShopBackground, BackgroundAdmin)
admin.site.register(Feedback, FeedbackAdmin)
