import urllib.request, urllib.parse
import json

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.core.urlresolvers import reverse
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, redirect
from django.utils.decorators import method_decorator
from django.utils import timezone

from authentication.forms import SignupForm
from main import settings as settings_project

from ads.decorators import shop_open
from ads.models import Advert, Region, AdvertPhoto, Shop, Billboard, Favorite, Division, ProjectSettings, ShopBackground
from ads.utils import translit, random_key, random_objects, ads_search, EmailSender
from authentication.models import MessageGroup
from .models import Feedback, Metro, County
from .forms import AdvertCreateForm, ShopCreateForm, BillboardCreateForm, BillboardUpdateForm
from django.views.generic import TemplateView, CreateView, DetailView, UpdateView
from django.db.models import Q


class HomeView(TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data()
        context['regions'] = Region.objects.all()
        adverts = Advert.objects.filter(status='accepted')
        if adverts:
            context['vips'] = random_objects(15, adverts.filter(vip=True))
        context['ups'] = adverts.filter(position__isnull=False).order_by('-position')[:15]
        return context


@method_decorator(login_required, name='dispatch')
class AdvertCreate(CreateView):
    template_name = 'advert_create.html'
    model = Advert
    form_class = AdvertCreateForm

    def get_context_data(self, **kwargs):
        context = super(AdvertCreate, self).get_context_data(**kwargs)
        context['images'] = AdvertPhoto.objects.filter(user=self.request.user, advert=None)
        return context

    def get_initial(self):
        self.initial.update({'phone': '%s' % self.request.user.profile.phone})
        return super(AdvertCreate, self).get_initial()

    def form_valid(self, form):
        settings = ProjectSettings.objects.last()
        advert = form.save(commit=False)
        advert.cost = int(form.cleaned_data.get('cost').replace(' ', ''))
        advert.owner = self.request.user
        advert.url_name = translit(form.cleaned_data['title'])
        video = form.cleaned_data.get('video')
        if video:
            advert.video = video.replace('watch?v=', 'embed/')
        advert.visitors = 0
        advert.today = 0
        advert.show = True
        advert.status = 'consideration'
        if not settings or not settings.checking_advert:
            advert.status = 'accepted'

        if form.cleaned_data.get('type') == 'offer':
            advert.single_division = None
        advert.save()
        images = AdvertPhoto.objects.filter(user=self.request.user, advert=None)
        for image in images:
            image.advert = advert
            image.save()
        return super(AdvertCreate, self).form_valid(form)

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, 'Объявление подано!')
        return reverse('profile:general')


def metro_load(request):
    if request.method == 'POST':
        region_id = request.POST.get('region_id')
        metro = Metro.objects.filter(region_id=region_id)
        return render(request, 'region_filter/metro_result.html', {'metro': metro})
    else:
        raise Http404()


def county_load(request):
    if request.method == 'POST':
        region_id = request.POST.get('region_id')
        county = County.objects.filter(region_id=region_id)
        return render(request, 'region_filter/county_result.html', {'county': county})
    else:
        raise Http404()


class AdvertUpdate(UpdateView):
    model = Advert
    form_class = AdvertCreateForm
    template_name = 'advert_create.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(AdvertUpdate, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AdvertUpdate, self).get_context_data()
        context['images'] = AdvertPhoto.objects.filter(user=self.request.user, advert_id=self.kwargs['pk'])
        return context

    def get_object(self, queryset=None):
        return get_object_or_404(Advert, pk=self.kwargs['pk'], owner=self.request.user)

    def form_valid(self, form):
        settings = ProjectSettings.objects.last()
        advert = form.save(commit=False)
        advert.cost = int(form.cleaned_data.get('cost').replace(' ', ''))
        if settings.checking_advert:
            advert.status = 'consideration'
        advert.url_name = translit(form.cleaned_data['title'])
        advert.multiple_division.clear()
        if form.cleaned_data['type'] == 'offer':
            advert.single_division = None
        else:
            advert.multiple_division.clear()
        images = AdvertPhoto.objects.filter(user=self.request.user, advert=None)
        for image in images:
            image.advert = advert
            image.save()
        advert.date = timezone.now()
        advert.show = True
        advert.save()
        return super(AdvertUpdate, self).form_valid(form)

    def get_success_url(self):
        advert = Advert.objects.get(pk=self.kwargs['pk'])
        return reverse('advert_detail', kwargs={'pk': self.kwargs['pk'], 'url_name': advert.url_name})


def advert_photo_upload(request):
    if request.method == 'POST':
        images = []
        advert_images = AdvertPhoto.objects.filter(user=request.user, advert__isnull=True).count()
        error = ''
        for file in request.FILES.getlist('images'):
            if advert_images < 10:
                images.append(AdvertPhoto.objects.create(user=request.user, photo=file))
                advert_images += 1
            else:
                error = 'max_size'
        return render(request, 'upload_result.html', {'images': images, 'error': error})


def advert_photo_delete(request):
    if request.method == 'POST':
        advert = AdvertPhoto.objects.get(id=int(request.POST.get('id')))
        advert.delete()
    return HttpResponse()


class AdvertDetail(DetailView):
    model = Advert
    template_name = 'advert_detail.html'
    current_date = timezone.now()
    date = current_date - timezone.timedelta(minutes=30)

    def get_object(self, queryset=None):
        obj = get_object_or_404(Advert, url_name=self.kwargs['url_name'], pk=self.kwargs['pk'])
        if obj.status == 'accepted' or obj.date <= self.date:
            if obj.last_visit:
                if int(obj.last_visit.strftime('%d')) != int(timezone.now().strftime('%d')):
                    obj.today = 0
            if not obj.visitors:
                obj.visitors = 1
            else:
                obj.visitors += 1
            if not obj.today:
                obj.today = 1
            else:
                obj.today += 1
            obj.last_visit = self.current_date
            obj.save()
        return obj

    def get_context_data(self, **kwargs):
        context = super(AdvertDetail, self).get_context_data()
        obj = Advert.objects.get(url_name=self.kwargs['url_name'], pk=self.kwargs['pk'])
        if obj.show and (obj.status == 'accepted' or obj.date <= self.date):
            context['show'] = True
        else:
            context['show'] = False
        try:
            shop = Shop.objects.get(owner=obj.owner, status='open', paid=True)
            context['shop_adverts'] = Advert.objects.filter(
                shop=shop, status='accepted', on_slider=True
            ).exclude(
                pk=obj.pk
            )
            context['shop'] = shop
        except Shop.DoesNotExist:
            pass
        if obj.single_division:
            context['similar_adverts'] = Advert.objects.filter(status='accepted',
                                                               single_division=obj.single_division.id).exclude(
                pk=obj.pk).distinct()
        else:
            context['similar_adverts'] = Advert.objects.filter(status='accepted',
                                                               multiple_division__in=obj.multiple_division.all()). \
                exclude(pk=obj.pk).distinct()
        try:
            if self.request.user.is_authenticated():
                favorite = Favorite.objects.get(user=self.request.user, adverts__in=[obj])
            else:
                cookie = self.request.COOKIES.get('fav')
                if cookie:
                    favorite = Favorite.objects.get(key=cookie, adverts__in=[obj])
                else:
                    favorite = False
            context['favorite'] = favorite
        except Favorite.DoesNotExist:
            pass
        try:
            if self.request.user.is_authenticated():
                context['group'] = MessageGroup.objects.filter(users=self.request.user).filter(
                    users=obj.owner).distinct()
        except MessageGroup.DoesNotExist:
            pass
        context['settings'] = ProjectSettings.objects.last()
        return context


class AdvertList(TemplateView):
    template_name = 'advert_list.html'

    def get_context_data(self, **kwargs):
        context = super(AdvertList, self).get_context_data()
        date = timezone.now() - timezone.timedelta(minutes=30)
        q = Q(date__lte=date) | Q(status='accepted')
        advert_list = Advert.objects.filter(show=True).filter(q)
        data = self.request.GET.get('search')
        if data:
            data = data.lower()
            advert_list = ads_search(advert_list, data)
        region = None
        if self.request.GET.get('region'):
            try:
                region = Region.objects.get(url_name=self.request.GET.get('region'))
            except Region.DoesNotExist:
                pass
            advert_list = advert_list.filter(region=region)

        if self.request.GET.get('category'):
            advert_list = advert_list.filter(
                Q(multiple_division__url_name__in=[self.request.GET.get('category')]) |
                Q(single_division__url_name__in=[self.request.GET.get('category')]))
        if self.request.GET.get('county'):
            advert_list = advert_list.filter(county__url_name=self.request.GET.get('county'))
        if self.request.GET.get('metro'):
            advert_list = advert_list.filter(metro__url_name=self.request.GET.get('metro'))
        if self.request.GET.get('act'):
            advert_list = advert_list.filter(act=self.request.GET.get('act'))
        if self.request.GET.get('type'):
            advert_list = advert_list.filter(type=self.request.GET.get('type'))
        paginator = Paginator(advert_list.order_by('-position'), 16)
        page = self.request.GET.get('page')
        try:
            adverts = paginator.page(page)
        except PageNotAnInteger:
            adverts = paginator.page(1)
        except EmptyPage:
            adverts = paginator.page(paginator.num_pages)

        context['adverts'] = adverts
        vips = advert_list.exclude(vip=False)
        context['vips'] = random_objects(3, vips)
        cookie = self.request.COOKIES.get('view')
        billboards = Billboard.objects.filter(region=region, status='accept', show=True)

        settings = ProjectSettings.objects.last()
        # billboard blocks
        if settings:
            context['settings'] = settings
            block_1 = random_objects(1, billboards.filter(block=1, user__profile__balance__gte=(
                settings.block_1 + settings.billboard)))
            context['block_1'] = self.show_plus(block_1, settings.block_1)

            block_2 = random_objects(1, billboards.filter(block=2, user__profile__balance__gte=(
                settings.block_2 + settings.billboard)))
            context['block_2'] = self.show_plus(block_2, settings.block_2)

            block_3 = random_objects(1, billboards.filter(block=3, user__profile__balance__gte=(
                settings.block_3 + settings.billboard)))
            context['block_3'] = self.show_plus(block_3, settings.block_3)

            block_4 = random_objects(1, billboards.filter(block=4, user__profile__balance__gte=(
                settings.block_4 + settings.billboard)))
            context['block_4'] = self.show_plus(block_4, settings.block_4)

        if cookie:
            context['class'] = cookie
        else:
            context['class'] = 'gallery'
        context['count'] = len(advert_list)
        context['categories'] = Division.objects.all()
        context['county'] = County.objects.filter(region__url_name=self.request.GET.get('region'))
        context['metros'] = Metro.objects.filter(region__url_name=self.request.GET.get('region'))
        return context

    # billboard shows plus one
    @staticmethod
    def show_plus(obj, cost):
        if obj:
            obj[0].shows += 1
            obj[0].save()
            obj[0].user.profile.balance -= cost
            obj[0].user.profile.save()
            return obj[0]


@method_decorator(login_required, name='dispatch')
class ShopCreateView(CreateView):
    model = Shop
    template_name = 'shop_create.html'
    form_class = ShopCreateForm

    def get_context_data(self, **kwargs):
        context = super(ShopCreateView, self).get_context_data()
        context['images'] = ShopBackground.objects.all()
        return context

    def get_initial(self):
        self.initial.update({
            'person': '%s %s' % (self.request.user.first_name, self.request.user.last_name),
            'phones': '%s' % self.request.user.profile.phone,
            'region': self.request.user.profile.region
        })
        return super(ShopCreateView, self).get_initial()

    def form_valid(self, form):
        shop = form.save(commit=False)
        shop.owner_id = self.request.user.id
        shop.status = 'close'
        adverts = Advert.objects.filter(owner_id=self.request.user.id, type='ready', act='sell')
        if form.cleaned_data.get('image', None):
            background = ShopBackground.objects.get(id=form.cleaned_data['image'])
            background = background.image
        else:
            background = None

        shop.background = background
        shop.save()

        for advert in adverts:
            advert.shop = shop
            advert.save()
        messages.add_message(self.request, messages.SUCCESS, 'Магазин успешно создан!')
        return super(ShopCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse('profile:general')


@method_decorator(login_required, name='dispatch')
class ShopUpdateView(UpdateView):
    model = Shop
    template_name = 'shop_create.html'
    form_class = ShopCreateForm

    def get_context_data(self, **kwargs):
        context = super(ShopUpdateView, self).get_context_data()
        context['images'] = ShopBackground.objects.all()
        return context

    def get_initial(self):
        self.initial.update({'person': '%s %s' % (self.request.user.first_name, self.request.user.last_name)})
        return super(ShopUpdateView, self).get_initial()

    def get_object(self, queryset=None):
        return Shop.objects.get(alias=self.kwargs['alias'])

    def form_valid(self, form):
        shop = form.save(commit=False)
        shop.owner_id = self.request.user.id
        image = form.cleaned_data.get('image', None)
        if image:
            background = ShopBackground.objects.get(id=image)
            background = background.image
            shop.background = background
        else:
            if shop.background:
                shop.background = None

        shop.save()
        adverts = Advert.objects.filter(owner_id=self.request.user.id, type='ready', act='sell')
        for advert in adverts:
            advert.shop = shop
            advert.save()
        return super(ShopUpdateView, self).form_valid(form)

    def get_success_url(self):
        return reverse('shop_detail', kwargs={'alias': self.get_object().alias})


@method_decorator(shop_open, name='dispatch')
class ShopDetailView(DetailView):
    model = Shop
    template_name = 'shop_detail.html'

    def get_object(self, queryset=None):
        return get_object_or_404(Shop, alias=self.kwargs['alias'])

    def get_context_data(self, **kwargs):
        context = super(ShopDetailView, self).get_context_data()
        context['adverts'] = Advert.objects.filter(owner_id=self.request.user.id, on_slider=True, show=True,
                                                   status='accepted')
        obj = self.get_object()
        settings = ProjectSettings.objects.last()
        if not obj.paid:
            context['not_paid'] = True
            if self.request.user.profile.balance >= settings.shop:
                action = True
            else:
                action = False
            context['enough'] = action
        context['project'] = settings
        return context


def shop_payment(request):
    if request.method == 'POST':
        settings = ProjectSettings.objects.last()
        profile = request.user.profile
        if profile.balance >= settings.shop:
            try:
                shop = Shop.objects.get(owner=request.user)
                if not shop.paid or shop.active_days < 7:
                    profile.balance -= settings.shop
                    profile.save()
                    shop.status = 'open'
                    shop.paid = True
                    shop.active_days += settings.active_days
                    shop.save()
                    result = settings.active_days
                else:
                    result = 'shop paid'
            except Shop.DoesNotExist:
                result = 'shop DoesNotExist'
        else:
            result = 'balance error'
        return HttpResponse(result)
    else:
        raise Http404()


def shop_status(request):
    if request.method == 'POST':
        status = {'true': 'open', 'false': 'close'}
        key = request.POST.get('status')
        shop = request.user.shop
        if shop:
            shop.status = status[key]
            shop.save()
        return HttpResponse(shop.get_status_display())
    else:
        raise Http404()


class ShopListView(TemplateView):
    template_name = 'shop_list.html'

    def get_context_data(self, **kwargs):
        context = super(ShopListView, self).get_context_data()
        cookie = self.request.COOKIES.get('view')
        context['regions'] = Region.objects.all()
        region = self.request.GET.get('region')
        shops = Shop.objects.filter(status='open', active_days__gte=1)
        if region and region != 'all':
            shops = shops.filter(region__url_name=region)
        context['shops'] = shops
        if cookie:
            context['class'] = cookie
        else:
            context['class'] = 'gallery'
        context['region_get'] = region
        return context


def advert_accept(request):
    if request.method == 'POST':
        advert = Advert.objects.get(pk=request.POST.get('id'))
        status = request.POST.get('status')
        reject_text = request.POST.get('solve_reject_text')
        if (request.user.profile.partner and request.user.profile.region == advert.region) \
                or request.user.profile.super_moderator:
            advert.status = status
            if status == 'rejected':
                advert.solve_reject_text = reject_text
            try:
                advert.shop = Shop.objects.get(owner=request.user)
            except Shop.DoesNotExist:
                pass
            advert.save()
            advert = Advert.objects.filter(region=request.user.profile.region, status='consideration').first()
            if not advert:
                messages.add_message(request, messages.SUCCESS, 'Все объявления отмодерированы.')
                return HttpResponse(reverse('profile:general'))
            else:
                return HttpResponse(reverse('advert_detail', kwargs={'url_name': advert.url_name, 'pk': advert.pk}))
        else:
            return HttpResponse('error')


def advert_reject(request):
    if request.method == 'POST':
        advert = Advert.objects.get(pk=request.POST.get('id'))
        if request.user.profile.partner and \
                (request.user.profile.region == advert.region or request.user.profile.super_moderator):
            if request.POST.get('message'):
                advert.status = 'rejected'
                advert.solve_reject_text = request.POST.get('message')
                advert.save()
                advert = Advert.objects.filter(region=request.user.profile.region, status='consideration').first()
                if not advert:
                    messages.add_message(request, messages.SUCCESS, 'Все объявления отмодерированы.')
                    return HttpResponse(reverse('profile:general'))
                else:
                    return HttpResponse(reverse('advert_detail', kwargs={'url_name': advert.url_name, 'pk': advert.pk}))
            else:
                return HttpResponse('error')


class CityFilter(TemplateView):
    template_name = 'advert_list.html'

    def get_context_data(self, **kwargs):
        context = super(CityFilter, self).get_context_data()
        region = get_object_or_404(Region, url_name=self.kwargs['city'])
        billboards = Billboard.objects.filter(region=region, status='accept', show=True)

        settings = ProjectSettings.objects.last()
        # billboard blocks
        if settings:
            block_1 = random_objects(1, billboards.filter(block=1, user__profile__balance__gte=(
                settings.block_1 + settings.billboard)))
            context['block_1'] = self.show_plus(block_1, settings.block_1)

            block_2 = random_objects(1, billboards.filter(block=2, user__profile__balance__gte=(
                settings.block_2 + settings.billboard)))
            context['block_2'] = self.show_plus(block_2, settings.block_2)

            block_3 = random_objects(1, billboards.filter(block=3, user__profile__balance__gte=(
                settings.block_3 + settings.billboard)))
            context['block_3'] = self.show_plus(block_3, settings.block_3)

            block_4 = random_objects(1, billboards.filter(block=4, user__profile__balance__gte=(
                settings.block_4 + settings.billboard)))
            context['block_4'] = self.show_plus(block_4, settings.block_4)

        try:
            adverts = Advert.objects.filter(region=region, status='accepted', show=True)
            vips = adverts.exclude(vip=False)
            context['vips'] = random_objects(3, vips)
            context['adverts'] = adverts.order_by('-position')
        except Advert.DoesNotExist:
            pass

        cookie = self.request.COOKIES.get('view')
        if cookie:
            context['class'] = cookie
        else:
            context['class'] = 'gallery'
        context['count'] = len(context['adverts'])
        context['region'] = region
        context['categories'] = Division.objects.all()
        return context

    # billboard shows plus one
    @staticmethod
    def show_plus(obj, cost):
        if obj:
            obj[0].shows += 1
            obj[0].save()
            obj[0].user.profile.balance -= cost
            obj[0].user.profile.save()
            return obj[0]


def set_cookie(request):
    if request.method == 'GET':
        view_type = request.GET.get('type')
        response = HttpResponse(view_type)
        response.set_cookie('view', '%s' % view_type)
        return response
    else:
        return HttpResponse()


def advert_favorite(request):
    if request.method == 'POST':
        response = HttpResponse()
        if request.user.is_authenticated():
            try:
                favorite = Favorite.objects.get(user_id=request.user.id)
            except Favorite.DoesNotExist:
                favorite = Favorite.objects.create(user_id=request.user.id)
        else:
            if request.COOKIES.get('fav'):
                try:
                    favorite = Favorite.objects.get(key=request.COOKIES.get('fav'))
                except Favorite.DoesNotExist:
                    favorite = Favorite.objects.create(key=random_key())
                    response.set_cookie('fav', '%s' % favorite.key)
            else:
                favorite = Favorite.objects.create(key=random_key())
                response.set_cookie('fav', '%s' % favorite.key)
        advert = Advert.objects.get(id=request.POST.get('id'))
        if favorite in advert.favorite.all():
            advert.favorite.remove(favorite)
            response.write('del')
        else:
            advert.favorite.add(favorite)
        advert.save()
        return response
    else:
        raise Http404()


class FavoriteView(TemplateView):
    template_name = 'favorite.html'

    def get_context_data(self, **kwargs):
        context = super(FavoriteView, self).get_context_data()
        favorite = False
        if self.request.user.is_authenticated():
            try:
                favorite = Favorite.objects.get(user_id=self.request.user.id)
            except Favorite.DoesNotExist:
                pass
        else:
            cookie = self.request.COOKIES.get('fav')
            if cookie:
                try:
                    favorite = Favorite.objects.get(key=cookie)
                except Favorite.DoesNotExist:
                    pass
        context['favorite'] = favorite
        return context


def search(request):
    if request.method == 'POST':
        data = request.POST.get('data')
        date = timezone.now() - timezone.timedelta(minutes=30)
        q = Q(date__lte=date) | Q(status='accepted')
        adverts = Advert.objects.filter(show=True).filter(q)
        print(adverts)
        data = data.lower()
        result = ads_search(adverts, data)
        return render(request, 'search_result.html', {'adverts': result})
    else:
        return Http404()


class CategoryView(TemplateView):
    template_name = 'advert_list.html'

    def get_context_data(self, **kwargs):
        context = super(CategoryView, self).get_context_data()
        category = get_object_or_404(Division, url_name=self.kwargs['category'])

        if self.kwargs['city'] != 'all':
            region = get_object_or_404(Region, url_name=self.kwargs['city'])
            adverts = Advert.objects.filter(region=region, status='accepted', show=True).filter(
                multiple_division__in=[category])
            billboards = Billboard.objects.filter(region=region, status='accepted', show=True)
            context['region'] = region
        else:
            adverts = Advert.objects.filter(status='accept', show=True).filter(
                Q(multiple_division__in=[category]) | Q(single_division__in=[category]))
            billboards = Billboard.objects.filter(region=None, status='accepted', show=True)

        context['adverts'] = adverts.order_by('-position')
        vips = adverts.exclude(vip=False)
        context['vips'] = random_objects(3, vips)

        settings = ProjectSettings.objects.last()
        # billboard blocks
        if settings:
            block_1 = random_objects(1, billboards.filter(block=1, user__profile__balance__gte=(
                settings.block_1 + settings.billboard)))
            context['block_1'] = self.show_plus(block_1, settings.block_1)

            block_2 = random_objects(1, billboards.filter(block=2, user__profile__balance__gte=(
                settings.block_2 + settings.billboard)))
            context['block_2'] = self.show_plus(block_2, settings.block_2)

            block_3 = random_objects(1, billboards.filter(block=3, user__profile__balance__gte=(
                settings.block_3 + settings.billboard)))
            context['block_3'] = self.show_plus(block_3, settings.block_3)

            block_4 = random_objects(1, billboards.filter(block=4, user__profile__balance__gte=(
                settings.block_4 + settings.billboard)))
            context['block_4'] = self.show_plus(block_4, settings.block_4)
        cookie = self.request.COOKIES.get('view')
        if cookie:
            context['class'] = cookie
        else:
            context['class'] = 'gallery'
        context['categories'] = Division.objects.all()
        context['current'] = self.kwargs['category']
        context['count'] = len(context['adverts'])
        return context

    # billboard shows plus one
    @staticmethod
    def show_plus(obj, cost):
        if obj:
            obj[0].shows += 1
            obj[0].save()
            obj[0].user.profile.balance -= cost
            obj[0].user.profile.save()
            return obj[0]


def vip(request):
    advert = Advert.objects.get(id=request.GET.get('id'))
    settings = ProjectSettings.objects.last()
    profile = request.user.profile
    if profile.balance >= settings.advert_vip:
        advert.vip = True
        advert.save()
        profile.balance -= settings.advert_vip
        profile.save()
        result = 'ok'
    else:
        result = 'balance_error'
    return HttpResponse(result)


def allotted(request):
    advert = Advert.objects.get(id=request.GET.get('id'))
    settings = ProjectSettings.objects.last()
    profile = request.user.profile
    if profile.balance >= settings.advert_allotted:
        advert.allotted = True
        advert.save()
        profile.balance -= settings.advert_allotted
        profile.save()
        result = 'ok'
    else:
        result = 'balance_error'
    return HttpResponse(result)


def f_search(request):
    advert = Advert.objects.get(id=request.GET.get('id'))
    settings = ProjectSettings.objects.last()
    profile = request.user.profile
    if profile.balance >= settings.advert_search:
        advert.f_search = True
        advert.save()
        profile.balance -= settings.advert_search
        profile.save()
        result = 'ok'
    else:
        result = 'balance_error'
    return HttpResponse(result)


def up_list(request):
    advert = Advert.objects.get(id=request.GET.get('id'))
    settings = ProjectSettings.objects.last()
    profile = request.user.profile
    if profile.balance >= settings.advert_up:
        advert.position = Advert.objects.max_position() + 1
        advert.save()
        profile.balance -= settings.advert_up
        profile.save()
        result = 'ok'
    else:
        result = 'balance_error'
    return HttpResponse(result)


def click_count(request):
    if request.method == 'POST':
        billboard_id = request.POST.get('id')
        billboard = Billboard.objects.get(id=billboard_id)
        billboard.clicks += 1
        billboard.save()
        settings = ProjectSettings.objects.last()
        billboard.user.profile.balance -= settings.billboard
        billboard.user.profile.save()
        return HttpResponse()
    else:
        raise Http404()


class Contacts(TemplateView):
    template_name = 'contacts.html'

    def post(self, request):
        project = ProjectSettings.objects.last()
        message = request.POST.get('message')
        text = 'Сообщение для администрации'
        name = request.POST.get('name')
        mail = request.POST.get('mail')
        context = {'name': name,
                   'mail': mail,
                   'message': message,
                   'text': text}
        subject_context = {'text': text}
        text_message = 'feedback/feedback.txt'
        html_message = 'feedback/feedback.html'
        subject = 'feedback/subject.html'
        recipients = [project.feedback_email]
        sender = settings_project.DEFAULT_FROM_EMAIL
        self.send_email(context, text_message, subject, recipients, sender, subject_context, html_message)
        self.create_feedback(name, mail, message)
        messages.add_message(request, messages.SUCCESS, 'Ваше сообщение успешно отправлено!',
                             'Мы свяжемся с Вами в ближайшее время')
        return HttpResponseRedirect(reverse('contacts'))

    @staticmethod
    def send_email(context, text_message, subject, recipients, sender, subject_context, html_message=None):
        email = EmailSender(context=context, text_message=text_message, subject=subject,
                            recipients=recipients, subject_context=subject_context,
                            sender=sender, html_message=html_message)
        email.send()

    @staticmethod
    def create_feedback(name, mail, message):
        Feedback.objects.create(name=name, mail=mail, message=message)


class Advertising(TemplateView):
    template_name = 'advertising.html'

    def get_context_data(self, **kwargs):
        context = super(Advertising, self).get_context_data(**kwargs)
        context['settings'] = ProjectSettings.objects.last()
        return context

    def post(self, request):
        project = ProjectSettings.objects.last()
        message = request.POST.get('message')
        text = 'Сообщение о рекламе'
        name = request.POST.get('name')
        mail = request.POST.get('mail')
        context = {'name': name,
                   'mail': mail,
                   'message': message,
                   'text': text}
        subject_context = {'text': text}
        text_message = 'feedback/feedback.txt'
        html_message = 'feedback/feedback.html'
        subject = 'feedback/subject.html'
        recipients = [project.feedback_email]
        sender = settings_project.DEFAULT_FROM_EMAIL
        self.send_email(context, text_message, subject, recipients, sender, subject_context, html_message)
        self.create_feedback(name, mail, message)
        messages.add_message(request, messages.SUCCESS, 'Ваше сообщение успешно отправлено!',
                             'Мы свяжемся с Вами в ближайшее время')
        return HttpResponseRedirect(reverse('advertising'))

    @staticmethod
    def send_email(context, text_message, subject, recipients, sender, subject_context, html_message=None):
        email = EmailSender(context=context, text_message=text_message, subject=subject,
                            recipients=recipients, subject_context=subject_context,
                            sender=sender, html_message=html_message)
        email.send()

    @staticmethod
    def create_feedback(name, mail, message):
        Feedback.objects.create(advertising=True, name=name, mail=mail, message=message)


class Terms(TemplateView):
    template_name = 'terms.html'


def registr_view(request):
    context = SignupForm
    return render(request, 'socialaccount/signup.html', {'form': context})


def advert_delete(request):
    if request.method == 'POST':
        pk = request.POST.get('pk')
        try:
            advert = Advert.objects.get(pk=pk)
            if request.user == advert.owner:
                pk = advert.pk
                advert.delete()
                return HttpResponse(pk)
            else:
                return HttpResponse('error')
        except Advert.DoesNotExist:
            return HttpResponse('error')
    else:
        raise Http404()


def advert_stop(request):
    if request.method == 'POST':
        pk = request.POST.get('pk')
        status = True if request.POST.get('status') == 'true' else False
        try:
            advert = Advert.objects.get(pk=pk)
            if advert.owner == request.user:
                advert.show = status
                advert.save()
                return HttpResponse(advert.pk)
        except Advert.DoesNotExist:
            pass
        return HttpResponse('ok')
    else:
        raise Http404()


def billboard_delete(request, pk):
    billboard = get_object_or_404(Billboard, pk=pk)
    if request.user == billboard.user:
        billboard.delete()
        return redirect('billboard_list')
    raise Http404()


@login_required()
def billboard_stop(request):
    if request.method == 'POST':
        pk = request.POST.get('pk')
        status = True if request.POST.get('status') == 'true' else False
        try:
            billboard = Billboard.objects.get(pk=pk)
            if billboard.user == request.user:
                billboard.show = status
                billboard.save()
                return HttpResponse(billboard.pk)
            else:
                return HttpResponse('error')
        except Billboard.DoesNotExist:
            pass
        return HttpResponse('error')
    else:
        raise Http404()


@login_required
def billboard_create_view(request):
    form = BillboardCreateForm()
    if request.method == 'POST':
        form = BillboardCreateForm(request.POST, request.FILES)
        if form.is_valid():
            data = {
                'secret': settings_project.SECRET_CAPTCHA_KEY,
                'response': request.POST.get('g-recaptcha-response', ''),
                'remoteip': request.META.get('REMOTE_ADDR')
            }
            data = urllib.parse.urlencode(data).encode('utf8')
            captcha = urllib.request.Request('https://www.google.com/recaptcha/api/siteverify', data)
            response = urllib.request.urlopen(captcha).read().decode('utf8', 'ignore')
            response = json.loads(response)
            result = response['success']
            if not result:
                form.add_error(None, 'Заполните капчу')
            else:
                billboard = form.save(commit=False)
                billboard.user = request.user
                billboard.save()
                messages.add_message(request, messages.SUCCESS, 'Вы успешно создали рекламный блок')
                return redirect('profile:general')
    settings = ProjectSettings.objects.last()
    return render(request, 'billboard/create.html', {'form': form, 'settings': settings})


@login_required
def billboard_update(request, pk):
    billboard = get_object_or_404(Billboard, pk=pk)

    if request.method == 'GET':
        form = BillboardUpdateForm(instance=billboard)

    else:
        form = BillboardUpdateForm(request.POST, request.FILES, instance=billboard)
        if form.is_valid():
            form.save()
            return redirect('billboard_list')

    settings = ProjectSettings.objects.last()
    return render(request, 'billboard/update.html', {'form': form, 'settings': settings})


@login_required
def billboard_list(request):
    context = {
        'objects': request.user.billboards.all()
    }
    return render(request, 'billboard/list.html', context)
