# coding=utf-8
import random

from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.db.models import Q


def translit(name):
    letters = [
        ('а', 'a'),
        ('б', 'b'),
        ('в', 'v'),
        ('г', 'g'),
        ('д', 'd'),
        ('е', 'e'),
        ('ё', 'jo'),
        ('ж', 'zh'),
        ('з', 'z'),
        ('и', 'i'),
        ('й', 'j'),
        ('к', 'k'),
        ('л', 'l'),
        ('м', 'm'),
        ('н', 'n'),
        ('о', 'o'),
        ('п', 'p'),
        ('р', 'r'),
        ('с', 's'),
        ('т', 't'),
        ('у', 'u'),
        ('ф', 'f'),
        ('х', 'h'),
        ('ц', 'c'),
        ('ч', 'ch'),
        ('ш', 'sh'),
        ('щ', 'shh'),
        ('ъ', ''),
        ('ы', 'y'),
        ('ь', ''),
        ('э', 'e'),
        ('ю', 'yu'),
        ('я', 'ya'),

    ]

    letters_list = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'

    result = []
    for letter in name.lower():
        if letter in letters_list:
            c = ([item for item in letters if item[0] == letter])
            result.append(c[0][1])
        else:
            result.append(letter)
    return ''.join(result).replace(' ', '_').replace(',', '')


def random_key():
    letter = 'abcdefghigklmnopqrstuvwxyz1234567890'
    return ''.join(random.choice(letter) for i in range(30))


def random_objects(quantity, objs):
    l = []
    if not objs:
        return l
    if len(objs) <= quantity:
        return objs
    objs = list(objs)
    for i in range(quantity):
        random_range = len(objs)
        if random_range > 0:
            r = random.randrange(0, random_range)
            obj = objs[r]
            if obj not in l:
                l.append(obj)
                objs.remove(obj)
        else:
            break
    return l


def ads_search(objs, search_text):
    l = []
    for text in search_text.split(' '):
        for obj in objs:
            if text in obj.title.lower().split(' '):
                l.append(obj.pk)
    objs = objs.filter(pk__in=l)
    return objs


def max_position(objs, field):
    object_list = objs.objects.all()
    return max(object_list**{field} for obj in object_list)


class EmailSender(object):

    def __init__(self, context, subject, text_message, sender, subject_context,
                 recipients=list(), html_message=None):
        self.context = context
        self.subject = subject
        self.path_to_text_message = text_message
        self.sender = sender
        self.recipients = recipients
        self.html_message = html_message
        self.subject_context = subject_context

    def get_subject(self):
        return render_to_string(self.subject, self.subject_context)

    def get_text_message(self):
        return render_to_string(self.path_to_text_message, self.context)

    def get_html_message(self):
        return render_to_string(self.html_message, self.context)

    def send(self):
        subject = self.get_subject()
        text_message = self.get_text_message()
        html_message = self.get_html_message()
        recipients = self.recipients
        sender = self.sender
        return send_mail(subject, text_message, sender, recipients, html_message=html_message)
