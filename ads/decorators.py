from django.http import Http404
from django.shortcuts import get_object_or_404

from ads.models import Shop


def shop_open(function):
    def wrap(request, *args, **kwargs):
        shop = get_object_or_404(Shop, alias=kwargs['alias'])
        if shop.status == 'open' and shop.paid or shop.owner == request.user:
            return function(request, *args, **kwargs)
        else:
            raise Http404()

    return wrap
