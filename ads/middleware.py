from ads.models import Region


class RegionMiddleware:
    def process_request(self, request):
        request.regions = Region.objects.all()
        return None
