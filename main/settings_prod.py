from .settings import *

DEBUG = True
ALLOWED_HOSTS = ['*']


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'biznesdeskdb',
        'USER': 'root',
        'PASSWORD': 'mysqladmin',
        'HOST': 'localhost',
        'PORT': '3306',
    }
}

