from django.contrib import admin
from .models import Profile
# Register your models here.


class ProfileAdmin(admin.ModelAdmin):
    class Media:
        js = ('assets/js/jquery.min.js',
              'assets/js/admin_profile.js')

admin.site.register(Profile, ProfileAdmin)
