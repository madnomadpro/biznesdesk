from datetime import timedelta

from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.http import HttpResponse, Http404
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.generic import DetailView, UpdateView
from ads.models import Advert, Shop, Billboard, ProjectSettings, Region
from authentication.forms import ProfileEditForm
from .models import Profile, MessageGroup, Message


@method_decorator(login_required, name='dispatch')
class ProfileView(DetailView):
    model = User
    template_name = 'profile.html'

    def get_object(self, queryset=None):
        return User.objects.get(pk=self.request.user.id)

    def get_context_data(self, **kwargs):
        context = super(ProfileView, self).get_context_data()
        context['adverts'] = Advert.objects.filter(owner_id=self.request.user.id)
        profile = self.request.user.profile
        if profile.partner or profile.super_moderator:
            date = timezone.now() + timedelta(minutes=30)
            m_adverts = Advert.objects.filter(status='consideration')
            if profile.super_moderator:
                context['m_adverts'] = m_adverts
            else:
                context['m_adverts'] = m_adverts.filter(region=profile.region, date__lte=date)
            if profile.super_moderator:
                context['billboards'] = Billboard.objects.filter(status='consideration')
            else:
                context['billboards'] = Billboard.objects.filter(region=profile.region, status='consideration')

        context['groups'] = MessageGroup.objects.filter(users=self.request.user)
        settings_advert = ProjectSettings.objects.last()
        if settings_advert:
            context['settings_advert'] = settings_advert.block_1 * 2
        context['regions'] = Region.objects.all()
        return context


class ProfileUpdate(UpdateView):
    model = Profile
    template_name = 'profile_edit.html'
    form_class = ProfileEditForm

    def get_object(self, queryset=None):
        return Profile.objects.get(user_id=self.request.user.id)

    def form_valid(self, form):
        if self.request.method == 'POST':
            data = form.cleaned_data
            user = User.objects.get(pk=self.request.user.id)
            user.first_name = data['first_name']
            user.last_name = data['last_name']
            user.save()
        return super(ProfileUpdate, self).form_valid(form)

    def get_success_url(self):
        return reverse('profile:general')


def send_message(request):
    if request.method == 'POST':
        group_id = request.POST.get('group')
        user_id = request.POST.get('id')
        message_text = request.POST.get('message')
        try:
            group = MessageGroup.objects.filter(users=request.user).get(users=user_id)
        except MessageGroup.DoesNotExist:
            group = None
        if group_id:
            group = MessageGroup.objects.get(id=int(group_id))
            message = Message.objects.create(group=group, user=request.user, message=message_text)
            data = message
        elif group:
            message = Message.objects.create(group=group, user=request.user, message=message_text)
            data = message
        else:
            user_id = int(user_id)
            if user_id != request.user.id:
                group = MessageGroup()
                group.save()
                group.users.add(request.user, User.objects.get(id=user_id))
                message = Message.objects.create(group=group, user=request.user, message=message_text)
                data = message
            else:
                data = 'error'
        return render(request, 'message_result.html', {'data': data})
    else:
        raise Http404()


def billboard_accept(request):
    if not request.user.profile.partner and not request.user.profile.super_moderator:
        raise Http404()
    if request.method == 'POST':
        billboard_id = request.POST.get('id')
        billboard = Billboard.objects.get(id=billboard_id)
        billboard.status = 'accept'
        billboard.save()
        return HttpResponse()
    else:
        raise Http404()


def billboard_reject(request):
    if not request.user.profile.partner and not request.user.profile.super_moderator:
        raise Http404()
    if request.method == 'POST':
        billboard = Billboard.objects.get(id=request.POST.get('id'))
        billboard.status = 'prohibited'
        billboard.save()
        return HttpResponse('ok')
    else:
        raise Http404()


def message_read(request):
    if request.method == 'POST':
        try:
            messages = Message.objects.filter(group_id=request.POST.get('id'), read=False).exclude(user=request.user)
            for message in messages:
                message.read = True
                message.save()
            return HttpResponse('ok')
        except MessageGroup.DoesNotExist:
            pass
    else:
        raise Http404()


def billboard_image_change(request):
    if request.method == 'POST':
        billboard_id = request.POST.get('id')
        image = request.FILES['billboard_image']
        billboard = Billboard.objects.get(id=billboard_id)
        billboard.image = image
        billboard.save()
        return HttpResponse(billboard.image.url)
    else:
        raise Http404()


def billboard_link_change(request):
    if request.method == 'POST':
        billboard_id = request.POST.get('id')
        link = request.POST.get('link')
        billboard = Billboard.objects.get(id=billboard_id)
        billboard.link = link
        billboard.save()
        return HttpResponse(link)
    else:
        raise Http404()


def billboard_region(request):
    if request.method == 'POST':
        billboard_id = request.POST.get('id')
        region_id = request.POST.get('region')
        if region_id != 'all':
            try:
                region = Region.objects.get(id=region_id)
            except Region.DoesNotExist:
                region = None
        else:
            region = None

        try:
            billboard = Billboard.objects.get(id=billboard_id)
            billboard.region = region
            billboard.save()
        except Billboard.DoesNotExist:
            region = None

        if region:
            return HttpResponse(region.name)
        elif region_id == 'all':
            return HttpResponse('Вся Россия')
        else:
            return HttpResponse('error')
