from django.contrib.auth.models import User
from django.core import validators
from django.db import models
from ads.models import Region


def content_photo_name(instance, filename):
    return '/'.join([instance.user.username, filename])


class Profile(models.Model):
    user = models.OneToOneField(User, related_name='profile')
    phone = models.CharField(max_length=16, validators=[
        validators.RegexValidator(r'^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$',
                                  u'Введите правильный номер телефона')])
    partner = models.BooleanField(default=False)
    super_moderator = models.BooleanField(default=False)
    region = models.OneToOneField(Region, related_name='profile_region', null=True, blank=True)
    photo = models.ImageField(upload_to=content_photo_name, null=True, blank=True)
    balance = models.DecimalField(default=0, max_digits=10, decimal_places=2)

    class Meta:
        verbose_name = 'Профиль'
        verbose_name_plural = 'Профили'

    def __str__(self):
        return '%s %s' % (self.user.first_name, self.user.last_name)


class MessageGroup(models.Model):
    users = models.ManyToManyField(User, related_name='message_group')


class Message(models.Model):
    user = models.ForeignKey(User, related_name='message')
    group = models.ForeignKey(MessageGroup, related_name='group')
    message = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    read = models.BooleanField(default=False)
