from django import forms
from django.core import validators
from .models import Profile


class SignupForm(forms.Form):
    first_name = forms.CharField(max_length=30, label='Имя')
    last_name = forms.CharField(max_length=30, label='Фамилия', required=False)
    phone = forms.CharField(
        validators=[validators.RegexValidator(
            r'^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$',
            u'Введите правильный номер телефона'
        )],
        label='Номер телефона')

    def signup(self, request, user):
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.save()
        Profile.objects.create(user=user, phone=self.cleaned_data['phone'], partner=False)


class ProfileEditForm(forms.ModelForm):
    first_name = forms.CharField(max_length=30, label='Имя')
    last_name = forms.CharField(max_length=30, label='Фамилия')

    class Meta:
        model = Profile
        exclude = ['user', 'region', 'partner', 'balance']
