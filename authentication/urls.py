from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.ProfileView.as_view(), name='general'),
    url(r'^edit/$', views.ProfileUpdate.as_view(), name='update'),
    url(r'^send_message/$', views.send_message, name='send_message'),
    url(r'^billboard_accept/$', views.billboard_accept, name='billboard_accept'),
    url(r'^message_read/$', views.message_read, name='message_read'),
    url(r'^billboard_image/$', views.billboard_image_change, name='billboard_image'),
    url(r'^billboard_link/$', views.billboard_link_change, name='billboard_link'),
    url(r'^billboard_region/$', views.billboard_region, name='billboard_region'),
    url(r'^billboard_reject/$', views.billboard_reject, name='billboard_reject'),
]
